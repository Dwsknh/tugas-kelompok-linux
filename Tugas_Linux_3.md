III. Configure the DE, and remove sysbar panel at the bottom of the screen

1. Setelah di download, device akan merestart
2. Setelah laptop/PC kembali menyala, maka akan terdapat beberapa pilihan terkait LXQt, lalu pilih Mutter
3. Device akan melakukan pemasangan DE LXQt
4. Setelah itu, untuk menghilangkan sysbar panel yaitu dengan cara :
    - Klik kanan pada sysbar
    - Klik Configure Panel
    - Klik Auto-Hide
5. Setelah mengikuti proses pada nomor 4 maka sysbar akan hilang dengan sendirinya dan akan muncul jika diperlukan