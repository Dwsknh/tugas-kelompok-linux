II. Use LXQt as desktop environment

1. Buka terminal pada Linux Ubuntu
2. Masukkan perintah:

    sudo apt install lxqt sddm

3. Masukkan password
4. Pilih OK pada package configuration
5. Selanjutnya pilih sddm
6. Jika proses telah selesai selanjutnya Restart Ubuntu >> klik power >> restart
7. Kemudian pada tampilan awal pilih Desktop Session LXQt Desktop
8. Masukkan password dan masuk pada Ubuntu
9. Selanjutnya pada welcome to LXQt pilih Mutter 
10. Klik OK
