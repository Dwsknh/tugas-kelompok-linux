IV. Create a service stated in this link
buat service / daemon dengan ketentuan sebagai berikut

1. Script untuk merekam semua keyboard key event
2. Jika keyboard key yang diinput adalah printable ASCII (0x20 - 0x7f) maka tuliskan ke file /tmp/keypressed.log
3. File /tmp/keypressed.log harus dapat dibaca oleh semua user
4. Script tersebut dibuat menjadi daemon menggunakan systemd

requirement:

shebang line #!/bin/sh

Distro Debian or Ubuntu

Service Manager systemd

Kendala :
Untuk no.4 belum mengerjakan dikarenakan terkendala pada pencarian script untuk merekam semua keyboard key event. 