V. Create a script to list HID and Storage Devices connected through USB

#!/bin/sh
sudo df -Th | grep media
lsblk | grep sd
lsusb -v
blkid
