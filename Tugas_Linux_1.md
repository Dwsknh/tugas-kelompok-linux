I. Install linux (any distro, debian-based is preferred)

Cara Instalasi Windows Subsystem for Linux
1. Buka Pengaturan
2. Pilih Apps
3. Klik Apps & features
4. Dibawah bagian "Related settings", klik pilihan Programs and Features
5. Klik pilihan Turn Windows Features on or off dari panel sebelah kiri
6. Check Windows Subsystem for Linux
7. Klik tombol OK
8. Klik tombol Restart now

Setelah menyelesaikan langkah-langkah diatas, environment akan dikonfigurasi untuk mengunduh dan menjalankan distro Linux di Windows Anda.

Installing Linux Ubuntu di Virtual Box
1. Install virtual box dichrome
2. Install distro ubuntu 
3. Membuka virtual box
4. Siapkan OS Linux Ubuntu dengan mendownload-nya
5. Buat Virtual Machine di aplikasi Virtual Box  :
    - Buka Virtual Box di PC atau laptop
    - Klik menu New
    - Nantinya kotak dialog Create Virtual Machine akan muncul
    - Kemudian isi Nama sistem operasi
    - Pilih lokasi pada Machine Folder sesuai yang diinginkan
    - Jika sudah, tentukan Type dan Version sesuai OS yang akan dipakai
    - Klik tombol Next
    - Tentukan Memory Size, disarankan untuk mengikuti sesuai yang direkomendasikan 
    - Pilih Create a virtual hard disk now di menu Hard Disk
    - Lalu tekan tombol Create
    - Berikutnya pilih opsi VHD (Virtual Hard Disk) dan klik Next
    - Untuk menu Storage on physical hard disk, pilih opsi Dynamically Allocated
    - Masukkan jumlah ukuran storage dan klik tombol Create
    - Setelah itu, Ubuntu Linux akan berhasil dibuat dan muncul di beranda Virtual Box
6. Konfigurasi Optical Drive
    - Pilih Ubuntu Linux dan klik tombol Start
    - Akan muncul kotak dialog untuk mengkonfigurasi Optical Disk Drive
    - Silakan klik tombol Folder
    - Kemudian klik tombol Add untuk menambahkan Optical Disk Drive baru
    - Menu Tambah Optical Drive Baru
    - Cari sistem operasi yang akan Anda gunakan, lalu klik tombol Open
    -Pilih OS Linux Ubuntu
    - Pilih OS yang dipakai dan tekan tombol Choose
    - Terakhir, klik tombol Start
7. Install Linux
    - Ketika Linux sudah berhasil dipasang, tunggu proses booting Linux selesai
    - Kemudian klik tombol Install Ubuntu
    - Opsi Install Ubuntu
    - Pada keyboard layout pilih English (US)
    - Lalu klik tombol Continue
    - Pilih Keyboard Layout English
    - Setelah itu pilih opsi Normal Installation pada Updates and other software
    - Pada Other options, centang opsi Download updates while installing Ubuntu
    - Pengaturan Updates Software
    - Klik tombol Continue
    - Pada Installation Type, pilih opsi Erase Disk and Install Ubuntu
    - Installation Type Ubuntu
    - Klik tombol Install Now
    - Sekarang silakan masukkan lokasi Anda dan klik tombol Continue
    - Pilih Lokasi Anda di Ubuntu
    - Terakhir, isi data pengguna, seperti nama PC dan password sesuai keinginan Anda
    - Masukkan Data Pengguna di Ubuntu
    - Tunggu sampai proses instalasi selesai
8. Cek hasil instalasi


